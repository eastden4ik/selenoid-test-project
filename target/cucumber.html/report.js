$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/features/Google.feature");
formatter.feature({
  "name": "Google test",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Google search page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Navigate to Google page",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Enter value \"Selenide\"",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Click search button",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "First link have \"Selenium\"",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});
Feature: Google test

  @Selenoid
  Scenario: Google search page
    Given Открытие страницы Google
    When Ввод значения "Selenide" в строке поиска
    And Нажатие кнопки Поиск
    Then Первая ссылка содержит: "Selenide: concise UI tests in Java"
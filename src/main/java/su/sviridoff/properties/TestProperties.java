package su.sviridoff.properties;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class TestProperties {

    private Properties properties = new Properties();
    private static TestProperties INSTANCE = null;

    private TestProperties() {
        try {
            if (System.getProperty("env") != null)
                properties.load(new FileInputStream(String.format("./%s.properties", System.getProperty("env"))));
            else if (System.getenv("env") != null)
                properties.load(new FileInputStream(String.format("./%s.properties", System.getenv("env"))));
            else
                throw new FileNotFoundException(String.format("No file: %s.properties", System.getenv("env")));
        } catch (IOException ex) {
            log.error("TestProperties: {}.", ex.getMessage());
        }
    }

    public static TestProperties getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TestProperties();
        }
        return INSTANCE;
    }

    public Properties getProperties() {
        return properties;
    }

}

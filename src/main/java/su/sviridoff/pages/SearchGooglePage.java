package su.sviridoff.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import su.sviridoff.properties.TestProperties;

import java.util.Properties;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchGooglePage {

    private static Properties properties = TestProperties.getInstance().getProperties();

    public SearchGooglePage open() {

        Selenide.open(properties.getProperty("app.url"));
        return this;
    }

    public void enterSearchParam(String value) {
        inputSearch.val(value);
    }

    public void clickSearch() {
        searchButton.click();
    }

    public void checkFirstResult(String value) {
        searchResults.get(0).text().contains(value);
    }

    private final SelenideElement inputSearch = $("input[name='q']");
    private final SelenideElement searchButton = $("input[name='btnK']");
    private final ElementsCollection searchResults = $$("div[class='g'] h3");

}
package su.sviridoff.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import su.sviridoff.pages.SearchGooglePage;

public class SearchSteps {

    private final SearchGooglePage searchGooglePage = new SearchGooglePage();

    @Given("^Открытие страницы Google$")
    public void navigateToPage() {
        searchGooglePage.open();
    }

    @When("^Ввод значения \"([^\"]*)\" в строке поиска$")
    public void enterValue(String value) {
        searchGooglePage.enterSearchParam(value);
    }

    @And("^Нажатие кнопки Поиск$")
    public void clickSearchButton() {
        searchGooglePage.clickSearch();

    }

    @Then("^Первая ссылка содержит: \"([^\"]*)\"$")
    public void checkFirstLink(String value) {
        searchGooglePage.checkFirstResult(value);
    }

}

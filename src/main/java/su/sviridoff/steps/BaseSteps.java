package su.sviridoff.steps;


import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.Selenide;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.openqa.selenium.remote.RemoteWebDriver;
import su.sviridoff.manager.BrowserEnum;
import su.sviridoff.manager.DriverManager;
import su.sviridoff.properties.TestProperties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.google.common.io.Files.*;

public class BaseSteps {

    private static Properties properties = TestProperties.getInstance().getProperties();

    @Before("@Selenoid")
    public void beforeScenario() {
        DriverManager.initDriver(BrowserEnum.CHROME_SELENOID);
    }

    @After("@Selenoid")
    public void afterScenario(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            screenshot();
            String sessionId = getSessionId();
            System.out.println("video.enabled: " + properties.getProperty("video.enabled"));
            Selenide.close();
            if ("true".equals(properties.getProperty("video.enabled"))) {
                Selenide.sleep(5000);
                attachAllureVideo(sessionId);
            }
        }

    }

    @Attachment(type = "image/png")
    private byte[] screenshot() throws IOException {
        File screenshot = Screenshots.takeScreenShotAsFile();
        return toByteArray(screenshot);
    }


    public static String getSessionId() {
        return ((RemoteWebDriver) getWebDriver()).getSessionId().toString();
    }


    public static void attachAllureVideo(String sessionId) {
        try {
            String selenoidUrl = String.format("http://%s:4444/", properties.getProperty("selenoid.url"));
            URL videoUrl = new URL(selenoidUrl + "/video/" + sessionId + ".mp4");

            InputStream is = getSelenoidVideo(videoUrl);
            Allure.addAttachment("Video", "video/mp4", is, "mp4");
            deleteSelenoidVideo(videoUrl);
        } catch (Exception e) {
            System.out.println("attachAllureVideo");
            e.printStackTrace();
        }
    }

    public static InputStream getSelenoidVideo(URL url) {
        int lastSize = 0;
        for (int i = 0; i < 20; i++) {
            try {
                int size = url.openConnection().getContentLength();
                System.out.println("Content-Length: " + size);
                System.out.println(i);
                if (size > lastSize || size == 19) {
                    lastSize = size;
                    //Thread.sleep(1000);
                    continue;
                }
                return url.openStream();
            } catch (Exception e) {
                System.out.println("checkSelenoidVideo");
                e.printStackTrace();
            }
        }

        return null;
    }

    public static void deleteSelenoidVideo(URL url) {
        try {
            HttpURLConnection deleteConn = (HttpURLConnection) url.openConnection();
            deleteConn.setDoOutput(true);
            deleteConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            deleteConn.setRequestMethod("DELETE");
            deleteConn.connect();
            deleteConn.disconnect();
        } catch (IOException e) {
            System.out.println("deleteSelenoidVideo");
            e.printStackTrace();
        }
    }
}

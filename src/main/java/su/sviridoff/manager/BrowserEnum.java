package su.sviridoff.manager;

public enum BrowserEnum {

    CHROME_WIN, FIREFOX_WIN, OPERA_WIN,
    CHROME_SELENOID, FIREFOX_SELENOID, OPERA_SELENOID

}

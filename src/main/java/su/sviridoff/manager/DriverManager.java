package su.sviridoff.manager;

import com.codeborne.selenide.Configuration;
import lombok.extern.slf4j.Slf4j;
import su.sviridoff.properties.TestProperties;

import java.util.Properties;

@Slf4j
public class DriverManager {

    private static Properties properties = TestProperties.getInstance().getProperties();

    public static void initDriver(BrowserEnum browser) {
        log.info("initDriver: {}", browser);
        switch (browser) {
            case CHROME_SELENOID: {
                log.info(String.format("http://%s:4444/wd/hub", properties.getProperty("selenoid.url")));
                Configuration.baseUrl = properties.getProperty("app.url");
                Configuration.remote = String.format("http://%s:4444/wd/hub", properties.getProperty("selenoid.url"));
                Configuration.browserSize = "1600x1024";
                Configuration.browserCapabilities.setCapability("enableVNC", Boolean.getBoolean(properties.getProperty("vnc.enable")));
                Configuration.browserCapabilities.setCapability("browserName", "chrome");
                Configuration.browserCapabilities.setCapability("browserVersion", "91.0");
                if ("true".equals(System.getProperty("video.enabled"))) {
                    Configuration.browserCapabilities.setCapability("enableVideo", Boolean.getBoolean(properties.getProperty("video.enabled")));
                    Configuration.browserCapabilities.setCapability("videoFrameRate", 24);
                }
                break;
            }
        }
    }


}

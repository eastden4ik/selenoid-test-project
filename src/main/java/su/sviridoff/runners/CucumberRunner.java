package su.sviridoff.runners;

import com.codeborne.selenide.Configuration;
import cucumber.api.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import su.sviridoff.properties.TestProperties;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {
                "src/test/features/",
        },
        plugin = {"io.qameta.allure.cucumber2jvm.AllureCucumber2Jvm"},
        glue = {"su.sviridoff"})

public class CucumberRunner {


//    private static TestProperties testProperties = TestProperties.getInstance();
//
//    @BeforeClass
//    public static void before() {
//        Configuration.remote = String.format("http://%s:4444/wd/hub", testProperties.getProperties().getProperty("selenoid.url"));
//        Configuration.browserSize = "1600x1024";
//        Configuration.browserCapabilities.setCapability("enableVNC", testProperties.getProperties().getProperty("vnc.enable"));
//        if ("true".equals(System.getProperty("video.enabled"))) {
//            Configuration.browserCapabilities.setCapability("enableVideo", testProperties.getProperties().getProperty("video.enabled"));
//            Configuration.browserCapabilities.setCapability("videoFrameRate", 24);
//        }
//    }

}